TestModule = angular.module("test-module", ['ngRoute'])

TestModule.config(
    ($routeProvider, $locationProvider) ->
        $routeProvider.when '/test/:id/', {
            templateUrl: '/static/templates/test.html',
            controller: 'TestController'
        }
        $routeProvider.when '/test/:id/result/', {
            templateUrl: '/static/templates/result.html',
            controller: 'ResultController'
        }
        $locationProvider.html5Mode {
            enabled: true
            requireBase: false
            rewriteLinks: true
        }
)
