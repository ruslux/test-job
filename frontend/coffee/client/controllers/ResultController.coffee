class ResultController
    constructor: (@$scope, @$rootScope, @$timeout, @$filter, @$api, @$sce, @$routeParams, @$location) ->
        @$scope.instance = this
        @testId = @$routeParams['id']
        @questions = @$api.getResult(@testId)

        @total = 0
        for question in @questions
            if question.user_answer.correct
                @total += question.score


    verboseUserAnswer: (question) ->
        switch question.type
            when 'textInput' then "\"#{question.user_answer.value}\""
            when 'radioButton' then "\"#{question.user_answer.option}. #{question.user_answer.value}\""
            when 'checkboxInput'
                verbose = []
                for answer in question.user_answer.value
                    verbose.push "\"#{answer.option}. #{answer.value}\""
                verbose.join ' и '


    verboseCorrectAnswer: (question) ->
        switch question.type
            when 'radioButton', 'checkboxInput'
                verbose = []
                for answer in question.answers
                    if answer.correct
                        verbose.push "\"#{answer.option}. #{answer.value}\""
                verbose.join ' и '
            when 'textInput'
                "\"#{question.correct_answers[0]}\""

TestModule.controller(
    'ResultController',
    [
        '$scope', '$rootScope', '$timeout', '$filter', '$api', '$sce', '$routeParams', '$location'
        ResultController
    ]
)