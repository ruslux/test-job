class TestController
    constructor: (@$scope, @$rootScope, @$timeout, @$filter, @$api, @$sce, @$routeParams, @$location) ->
        @$scope.instance = this
        @testId = @$routeParams['id']
        @answers = {}
        @questions = @$api.getQuestions(@testId)
        @hasAllAnswers = false

    checkFill: =>
        @hasAllAnswers = false
        request = {}

        for item in @questions
            answer = @answers[item.checksum]
            if answer?['text']
                request[item.checksum] = answer['text']
            else if answer?['radio']
                request[item.checksum] = answer['radio']
            else if answer?['checkbox']
                options = []
                for key, value of answer['checkbox']
                    if value
                        options.push key
                if options.length
                    request[item.checksum] = options

        if Object.keys(request).length == @questions.length
            @hasAllAnswers = true
            @request = request

    submit: =>
        if @hasAllAnswers
            @$api.sendAnswers(@request)
            #.success (data) =>
            @$location.path('/test/1/result/')

TestModule.controller(
    'TestController',
    [
        '$scope', '$rootScope', '$timeout', '$filter', '$api', '$sce', '$routeParams', '$location'
        TestController
    ]
)