TestModule.filter 'correctUserAnswers', ->
    (question_list, correct) ->
        filtered = []
        for question in question_list
            if question.user_answer.correct is correct
                filtered.push question
        filtered