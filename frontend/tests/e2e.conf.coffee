exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub'
  baseUrl: 'http://localhost:8000/'
  capabilities: {
    'browserName': 'chrome'
  }
  rootElement: '#application'
  specs: [
      'tests.js'
  ]
}