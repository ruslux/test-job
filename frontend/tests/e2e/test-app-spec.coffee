By = protractor.By

class TestQuestionsPage
    prepare: ->
        browser.get('/test/1/')
        browser.waitForAngular()
        this

    checkNoRedirect: ->
        expect(browser.getLocationAbsUrl()).toMatch('/test/1/')
        this

    checkQuestionsData: ->
        questionsList = element.all(By.repeater('question in instance.questions'))
        expect(questionsList.count()).toEqual(5)
        this

    checkEndTestButtonIsHidden: ->
        expect($('#endTestButton').isDisplayed()).toBeFalsy()
        this

    checkEndTestButtonIsShowed: ->
        expect($('#endTestButton').isDisplayed()).toBeTruthy()
        this


    fillAnswers: ->
        questionsList = element.all(By.repeater('question in instance.questions'))
        for num in [0..4]
            do (num) ->
                testInput = questionsList.get(num).all(By.tagName('input')).get(0)
                testInput.getAttribute('type').then (attr) ->
                    switch attr
                        when 'radio', 'checkbox'
                            testInput.click()
                        when 'text'
                            testInput.sendKeys('someText')
        this

    resetAnswers: ->
        questionsList = element.all(By.repeater('question in instance.questions'))
        for num in [0..4]
            do (num) ->
                testInput = questionsList.get(num).all(By.tagName('input')).get(0)
                testInput.getAttribute('type').then (attr) ->
                    switch attr
                        when 'text'
                            testInput.clear()
                        when 'checkbox'
                            testInput.click()
        this


describe 'angularjs test app questions page', ->
    it 'resolve tests', ->
        new TestQuestionsPage()
            .prepare()
            .checkNoRedirect()
            .checkQuestionsData()
            .checkEndTestButtonIsHidden()
            .fillAnswers()
            .checkEndTestButtonIsShowed()
            .resetAnswers()
            .checkEndTestButtonIsHidden()
