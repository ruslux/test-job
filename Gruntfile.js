/*global module:false*/
module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        // Metadata.
        pkg: grunt.file.readJSON('package.json'),
        banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
        '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
        '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
        '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
        ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
        // folders config
        frontend: 'frontend',
        he: '<%= frontend %>/coffee',
        compiled: '<%= frontend %>/compiled',
        libs: '<%= frontend %>/libs',
        styles: '<%= frontend %>/styles',
        static: 'static/',
        compiled_tests: '<%= frontend %>/compiled/tests',
        tests: '<%= frontend %>/tests',
        // Task configuration.
        concat: {
            options: {
                banner: '<%= banner %>',
                stripBanners: true
            },
            libsMinified: {
                src: [
                    '<%= libs %>/angular/*.min.js',
                    '<%= libs %>/angular-route/*.min.js',
                    //'<%= libs %>/bootstrap/dist/js/*.min.js'
                ],
                dest: '<%= static %>/js/libs.min.js'
            },
            app: {
                src: [
                    '<%= compiled %>/**/*.js'
                ],
                dest: '<%= static %>/js/app.min.js'
            },
            css: {
                src: [
                    '<%= libs %>/bootstrap/dist/css/*.min.css',
                    '<%= styles %>//css/*.css',
                ],
                dest: '<%= static %>/css/style.min.css'
            }
        },
        uglify: {
            options: {
                banner: '<%= banner %>'
            },
            dist: {
                src: '<%= concat.dist.dest %>',
                dest: 'dist/<%= pkg.name %>.min.js'
            }
        },
        watch: {
            gruntfile: {
                files: '<%= jshint.gruntfile.src %>',
                tasks: ['jshint:gruntfile']
            },
            lib_test: {
                files: '<%= jshint.lib_test.src %>',
                tasks: ['jshint:lib_test', 'nodeunit']
            }
        },
        coffee: {
            compileBare: {
                options: {
                    bare: true
                },
                files: {
                    '<%= compiled %>/app.js': [
                        '<%= he %>/client/settings/*.coffee',
                        '<%= he %>/client/services/*.coffee',
                        '<%= he %>/client/directives/*.coffee',
                        '<%= he %>/client/filters/*.coffee',
                        '<%= he %>/client/controllers/*.coffee'
                    ],
                    '<%= compiled_tests %>/e2e.conf.js': [
                        '<%= tests %>/e2e.conf.coffee'
                    ],
                    '<%= compiled_tests %>/tests.js': [
                        '<%= tests %>/e2e/*.coffee'
                    ]
                }
            }
        },
        copy: {
            main: {
                files: [
                    {expand: true, src: ['<%= libs %>/bootstrap/dist/fonts/*'], dest: '<%= static %>/fonts/'}
                ]
            }
        },
        protractor: {
            e2e: {
                options: {
                    configFile: "<%= compiled_tests %>/e2e.conf.js",
                    args: {}
                }
            }
        }
    });

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-coffee');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    //grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-protractor-runner');


    // Default task.
    grunt.registerTask('debug',
        [
            'coffee',
            'concat:libsMinified',
            'concat:app',
            'concat:css',
            'copy:main'
        ]
    );

    grunt.registerTask('test',
        [
            'coffee',
            'concat:libsMinified',
            'concat:app',
            'concat:css',
            'copy:main',
            'protractor'
        ]
    )
};
