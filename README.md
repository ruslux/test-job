### Установка back-end (Ubuntu 12.04+)

```bash
mkdir ruslux-test
cd ruslux-test
git clone https://ruslux@bitbucket.org/ruslux/test-job.git src
apt-get install python3 python-pip python-virtualenv
virtualenv --no-site-packages --distribute -p /usr/bin/python3 env
source env/bin/python
pip install -r src/requirements
```

### Установка и сборка front-end
```bash
npm install
bower install
grunt debug
```

### Запуск сервера
```bash
python src/manage.py runserver 8000
```

### Страница с тестом
http://localhost:8000/test/1/

### Тестирование front-end
```bash
grunt test
```

### Комментарии к заданию
* необходимо вычитать тестовое задание, конкретно фронтенд - много несостыковок. Конкретно: ответ сервера какой-то 
* косячный, пришлось доработать. Если это тест на смекалку, то мое личное мнение - такой ответ необходимо доводить до
* ума: убрать избыточность данных, форматы ответов не идентичны (название полей, опущен correct в answers)

* не стал делать согласование числительных `5 правиль(ных/ный)`

* Не уверен, что это правильно, употреблять так рядом: `Правильный ответ: "13" — правильно`