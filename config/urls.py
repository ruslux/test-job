from django.conf.urls import url, include
from django.conf.urls.static import static

from config import settings
from apps.test.views import HomeView


urlpatterns = [
    url(r'^', HomeView.as_view(), name='angular'),
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
